import pymongo
from mongoengine import connect, DoesNotExist, MultipleObjectsReturned
from helper import config as cfg


class MongoDBManager:
    """[summary]

    """

    def __init__(self):
        """[summary]
        """
        self.db_name = cfg.db_name
        self.db_port = int(cfg.db_port)
        self.db_host = cfg.db_host
        self.db_url = ("mongodb://" + self.db_host + ":" +
                       str(self.db_port) + "/")
        self.db_client = None
        self.db = None
        self.collection = None
        self.collection_name = cfg.collection_name
        self.dblist = None
        self.set_up_db()
        self.status = 200


    def establish_client_connection(self):
        """[summary]
        """
        try:
            cfg.logger.info("ESTABLISHED CONNECTION WITH DB AT {}:{}".format(
                self.db_host, self.db_port
            ))
            self.db_client = pymongo.MongoClient(self.db_host, self.db_port)
        except Exception as e:
            gv.cfg.logger.error(e)

    def create_db_connection(self):
        """[summary]
        """
        try:
            if self.db_client is not None:
                # If no exists
                if not self.check_database_exists():
                    cfg.logger.info("The new db created!")

                self.db = self.db_client[self.db_name]
                connect(self.db_name, host=cfg.host, port=int(cfg.db_port))
                self.status = 200
            else:
                cfg.logger.warning("Create a client connection first!")
                self.status = 405
        except Exception as e:
            cfg.logger.error(e)
            self.status = 404

    def set_up_db(self, collection_name=None):
        """[summary]

        :param collection_name: [description], defaults to None
        :type collection_name: [type], optional
        """
        try:
            # Establish connection
            self.establish_client_connection()
            # Create new dbc
            self.create_db_connection()
            # Create new collection
            if collection_name is None:
                collection_name = self.collection_name
            self.create_new_collection_instance(collection_name=collection_name)
        except Exception as e:
            cfg.logger.error(e)

    def check_db(self):
        """Checks if dbManager has started. If not, initialize dbManager.
        """
        try:
            if self.db_client is None:
                # If no exists
                if not self.check_database_exists():
                    cfg.logger.info("The new db created!")
                self.establish_client_connection()
                self.db = self.db_client[self.db_name]
                connect(self.db_name, host=cfg.host, port=int(cfg.db_port))
        except Exception as e:
            cfg.logger.error(e)
            self.status = 500

    def create_new_collection_instance(self, collection_name):
        """[summary]

        :param collection_name: [description]
        :type collection_name: [type]
        """
        try:
            # If no exists
            if self.db is not None:
                self.collection = self.db[collection_name]
                self.status = 200
            else:
                cfg.logger.warning("The connection to the db is not established!")
                self.status = 405
        except Exception as e:
            cfg.logger.error(e)
            self.status = 404
        return

    def check_database_exists(self):
        """[summary]

        :return: [description]
        :rtype: [type]
        """
        try:
            response = True
            dblist = self.db_client.list_database_names()
            if self.db_name not in dblist:
                response = False
                self.status = 200
        except Exception as e:
            cfg.logger.error(e)
            self.status = 404
            response = None
        return response

    """
    DB MANAGEMENT

    """

    def export_database(self):
        """[summary]

        :return: [description]
        :rtype: [type]
        """
        collection_names = self.db.list_collection_names()
        db_output = {"collections": []}
        # iterate over the list of collection names
        for collection_name in collection_names:
            collection_output = {collection_name: []}
            cursor = self.db[collection_name].find({})
            for document in cursor:
                # Convert to string
                collection_output[collection_name].append(document)
            db_output["collections"].append(collection_output)
        self.status = 200
        return db_output

    def import_db(self, db_json):
        """

        :param db_json:
        :type db_json:
        """
        try:
            if self.db is not None:
                # Should collections be cleared before import?
                # Or do we append to the actual content of the db?
                for collection in db_json["collections"]:
                    for collection_name, documents in collection.items():
                        db_collection = self.db[collection_name]
                        db_collection.insert_many(documents)
                        self.collection = db_collection
                cfg.logger.info("DB imported succesfully")
                self.status = 200
            else:
                cfg.logger.warning("The connection to the db is not established!")
                self.status = 405
        except Exception as e:
            cfg.logger.error(e)
            self.status = 404