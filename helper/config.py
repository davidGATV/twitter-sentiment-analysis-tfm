import os
from helper.custom_log import init_logger


host = "192.168.0.22"
port = "5003"

parent_dir = "review_dir"
tweets_output = os.path.join(parent_dir, "tweets_data.json")
filename = os.path.join(parent_dir, "tweets_user.csv")

# Database Parameters
db_name = "twitter_db"
db_port = "27017"
db_host = "localhost"
collection_name = "tweets"

# Twitter Data
consumer_key = ""
consumer_secret = ""
access_token = ""
access_token_secret = ""

# Sentiment Analysis model
sa_model = "vader" #vader
logger = init_logger(__name__, testing_mode=False)
