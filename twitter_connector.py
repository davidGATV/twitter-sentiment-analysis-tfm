import tweepy as tw
import os
import json
from helper import config as cfg

# ---------------------------------------------------------------------

consumer_key = ""
consumer_secret = ""
access_token = ""
access_token_secret = ""
# ---------------------------------------------------------------------


def connect_twitter():
    try:
        cfg.logger.info("Connecting to Twitter API ... ")
        auth = tw.OAuthHandler(consumer_key, consumer_secret)
        auth.set_access_token(access_token, access_token_secret)
        api = tw.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True,
                     retry_count=10, retry_delay=5, retry_errors=5)
    except Exception as e:
        cfg.logger.error(e)
        api = None
    return api


# Collect tweets
def collect_tweets(api, search_words, date_since, items=1000, lang='en'):
    try:
        # Collect tweets
        tweets = tw.Cursor(api.search,
                           lang=lang,
                           q=(search_words),
                           since=date_since,
                           include_entities=True,
                           monitor_rate_limit=True,
                           wait_on_rate_limit=True,
                           tweet_mode='extended').items(items)
    except Exception as e:
        cfg.logger.error(e)
        tweets = None
    return tweets


def write_json(data, csv_file):
    try:
        # Check if exists
        if not os.path.exists(csv_file):
            data = [data]
            with open(csv_file, 'w', newline='', encoding='utf-8') as outfile:
                outfile.write(json.dumps(data) + '\n')
                #writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
                #writer.writeheader()
                #writer.writerow(data)
        else:
            with open(csv_file) as f:
                old_data = json.load(f)
            old_data.append(data)
            with open(csv_file, 'w') as f:
                json.dump(old_data, f)
    except Exception as e:
        cfg.logger.error(e)
    return



